function getActiveTab() {
	return browser.tabs.query({active: true, currentWindow: true});
}

function onButtonClick() {
	getActiveTab().then((tabs) => {
		browser.tabs.sendMessage(tabs[0].id, {keb_unlock: true})
	});
}
browser.browserAction.onClicked.addListener(onButtonClick);
